
(* This function block will open the CAN port *)
FUNCTION_BLOCK sBus_MasterDriver
	
	sBusOpen(enable := xEnable , baud_rate := usiBaudRate  , cob_anz := uiMessages , error_adr := ADR(uiErrorAddress) , device := ADR(strNetId) , info :=0 );
	uiStatus:= sBusOpen.status;
	udiHandle:=sBusOpen.us_ident;
	
END_FUNCTION_BLOCK
