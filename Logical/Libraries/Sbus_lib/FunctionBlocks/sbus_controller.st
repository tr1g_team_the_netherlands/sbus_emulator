
(* this functionblock is used to control the drive.*)
(* the input contains an array of 3 PO int and the output contains an array of 3 PI ints*)
FUNCTION_BLOCK sbus_controller
	splitTheInt(aiPo[0],internal.byLowPo[0],internal.byHighPo[0]);
	splitTheInt(aiPo[1],internal.byLowPo[1],internal.byHighPo[1]);
	splitTheInt(aiPo[2],internal.byLowPo[2],internal.byHighPo[2]);
	
	aiPi[0] := jointTheBytes(internal.byLowPi[0],internal.byHighPi[0]);
	aiPi[1] := jointTheBytes(internal.byLowPi[1],internal.byHighPi[1]);
	aiPi[2] := jointTheBytes(internal.byLowPi[2],internal.byHighPi[2]);
	
	internal.abDataPo[0] := internal.byHighPo[0];
	internal.abDataPo[1] := internal.byLowPo[0];
	internal.abDataPo[2] := internal.byHighPo[1];
	internal.abDataPo[3] := internal.byLowPo[1];
	internal.abDataPo[4] := internal.byHighPo[2];
	internal.abDataPo[5] := internal.byLowPo[2];
	internal.abDataPo[6] := internal.byHighPo[3];
	internal.abDataPo[7] := internal.byLowPo[3];
	
	IF uiMasterStatus = 0 THEN 
(*--------------------------------------------------------------------------------------------------*)		
		CASE internal.state OF
			
			0:	// CANwrite
				internal.CANwrite_0
				(
					enable:=1,
					us_ident:=udiHandle,
					can_id:=Sbus_IdPo(uiMotorNumber),
					data_adr:=ADR(internal.abDataPo),
					data_lng:=6 (*control word commands are only 6 bytes messages*)
				);		
				IF internal.CANwrite_0.status = ERR_OK THEN
					RandDmessage := 'Write actions without errors';
					internal.state := 10;
				ELSE
					RandDmessage := 'There went somting wrong with the write action';		
				END_IF
				uiStatus := internal.CANwrite_0.status;
			
			10:	// CANread
				internal.CANread_0
				(
					enable :=1,
					us_ident :=udiHandle,
					can_id :=Sbus_IdPi(uiMotorNumber),
					data_adr :=ADR(abDataPi)
				);
				IF internal.CANread_0.status = ERR_OK THEN
					RandDmessage := 'Read actions without errors';
					internal.byHighPi[0] := abDataPi[0];
					internal.byLowPi[0]	:= abDataPi[1];
					internal.byHighPi[1] := abDataPi[2];
					internal.byLowPi[1]	:= abDataPi[3];
					internal.byHighPi[2] := abDataPi[4];
					internal.byLowPi[2]	:= abDataPi[5];
					internal.byHighPi[3] := abDataPi[6];
					internal.byLowPi[3]	:= abDataPi[7];
					internal.state := 0;
				ELSE
					RandDmessage := 'There went somting wrong with the read action';		
				END_IF
				uiStatus := internal.CANread_0.status;
			
		END_CASE
		
	END_IF
				
END_FUNCTION_BLOCK
