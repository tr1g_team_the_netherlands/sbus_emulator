
FUNCTION_BLOCK sBus_MasterDriver (*This function block will open the CAN port*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		xEnable : BOOL; (*This function block is only executed if enable is <> 0.*)
		strNetId : STRING[80]; (*Address of a string which clearly describes the CAN interface.*)
		usiBaudRate : USINT; (*Baudrate of the sbus (50 = 500 kbtis/sec)*)
		uiMessages : UINT; (*resvere memory for this amount of messages*)
	END_VAR
	VAR_OUTPUT
		uiStatus : UINT; (*Error number  (0 = no error). error number is an Enum of the CAN_lib*)
		udiHandle : UDINT; (*This handle is used for communication with other function blocks*)
	END_VAR
	VAR
		uiErrorAddress : UINT; (*Pointer to an error variable (UINT)*)
		sBusOpen : CANopen;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK sBus_info (*receive information of the bus*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		xEnable : BOOL;
		udiHandle : DINT;
	END_VAR
	VAR_OUTPUT
		uiStatus : UINT;
		structInfo : INFO_typ;
	END_VAR
	VAR
		CANinfo_0 : CANinfo;
		infoStruct : INFO_typ;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION jointTheBytes : INT (*funtion to join 2 bytes in one int*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		byHigh : BYTE;
		byLow : BYTE;
	END_VAR
	VAR
		temp : INT;
	END_VAR
END_FUNCTION

FUNCTION splitTheInt : BOOL (*function to split 1 int to 2 bytes*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		iSource : INT;
	END_VAR
	VAR_IN_OUT
		byLow : BYTE;
		byHigh : BYTE;
	END_VAR
END_FUNCTION

FUNCTION_BLOCK sbus_controller (*this functionblock is used to control the drive.*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		aiPo : ARRAY[0..2] OF INT;
		udiHandle : UDINT;
		uiMotorNumber : UINT;
		uiMasterStatus : UINT;
	END_VAR
	VAR_OUTPUT
		aiPi : {REDUND_UNREPLICABLE} ARRAY[0..2] OF INT;
		RandDmessage : STRING[80];
		uiStatus : UINT;
		abDataPi : ARRAY[0..7] OF USINT;
	END_VAR
	VAR
		internal : sbus_controller_internal_typ;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION Sbus_IdPi : UINT (*function to convert motornumer to sbus COB pi range*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		uiMotorNumber : UINT;
	END_VAR
END_FUNCTION

FUNCTION Sbus_IdPo : UINT (*function to convert motornumer to sbus COB po range*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		uiMotorNumber : UINT;
	END_VAR
END_FUNCTION
