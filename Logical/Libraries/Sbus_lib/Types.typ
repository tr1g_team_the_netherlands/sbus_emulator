
TYPE
	CAN_lib_ErrorNumbers : 
		(
		NO_ERROR := 0, (*No error has occurred.*)
		FBK_STILL_ACTIVE := 1, (*The FBK is still active.*)
		NAME_NOT_AVAILIBLE := 3019, (*module_name module is not available.*)
		DEVICE_NOT_ALLOWED := 3090, (*device != 0 is not allowed in the base system V1.10.*)
		ERR_DEVICEDESCRIPTION := 8252, (*Invalid interface description (device string).*)
		ERR_CAN_NOENTRY := 8800, (*Not enough communication objects (COBs) as specified in CANopen() and CANMulOpen() (increase cob_anz ).*)
		ERR_CAN_NOMEM := 8801, (*Not enough memory available for resources.*)
		ERR_CAN_WRIDENT := 8802, (*Wrong identifier*)
		ERR_CAN_COBUSED := 8803, (*Communication objects (COBs) identifier is already being used in the system.*)
		ERR_CAN_WRCANID := 8804, (*Wrong CAN identifier: Only standard CAN identifiers (11 bits) up to 2047 are permitted*)
		ERR_CAN_WRUSERID := 8805, (*Incorrect user ID us_ident transferred, or the FBK cannot be used after a CANMulOpen.*)
		ERR_CAN_COBDEF := 8806, (*A COB with the specified CAN identifier has already been defined in the system*)
		ERR_CAN_WRDEF := 8807, (*CAN: False CAN identifier definition *)
		ERR_CAN_WRHDLE := 8808, (*COB management (FBK memory) destroyed*)
		ERR_CAN_BUSY := 8809, (*CAN operation with the same CAN identifier is busy*)
		ERR_CAN_QFULL := 8810, (*Transfer queue full: Bus not connected or no active CAN node. A read identifier must be defined on at least one node (using CANdftab) in order for the controller to be active.*)
		ERR_CAN_BUSOFF := 8811, (*CAN bus is BusOff. Different baud rate on different nodes?*)
		ERR_CAN_NOHIGH := 8812, (*No more buffers available: All buffers (max. 13) in the controller have already been defined and assigned.*)
		ERR_CAN_NOTINST := 8813, (*CAN driver not installed: CAN2000.BR not loaded (needs to be transferred to target system)*)
		ERR_CAN_WRLNG := 8814, (*Data type of event PV is not BYTES or the length of a write PV is greater than 8 bytes: CAN FBKs can only send data of length 8 bytes to the CAN bus.*)
		ERR_CAN_BDRATE := 8815, (*CANopen()/CANMulOpen(): Illegal baud rate or wrong operating system version for extended mode (V2.20 or higher is required).*)
		ERR_CAN_COBANZ := 8816, (*cob_anz in CANopen()/CANMulOpen() incorrect: cob_no must be less than 2048 and greater than 0.*)
		ERR_CAN_NOCONTR := 8817, (*No CAN controller found: Error in the device string of the FBK CANopen / CANMulOpen or the CAN controller is not ready (hardware may not be present)*)
		ERR_CAN_HTLXINF := 8818, (*Error analyzing hardware*)
		ERR_CAN_HTLQUIT := 8819, (*Error in interrupt handling*)
		ERR_CAN_NOFUNC := 8820, (*Error in interrupt handling*)
		ERR_CAN_WRUSRTYP := 8821, (*user_typ in CMSinit() is incorrect: user_typ must be 0 (client) or 1 (server).*)
		ERR_WRCMSOBJ := 8822, (*CMS object incorrectly defined: Syntax error in the CMS data object for CMSinit() (see cms_entry).*)
		ERR_CAN_WRSTRUCT := 8823, (*Invalid CMS structure: The structure is too complex (only simple structures are permitted).*)
		ERR_CAN_WRMODE := 8824, (*Incorrect value for cms_mode transferred to the CMSmain() function (valid value: 2)*)
		ERR_CAN_NO_NODE := 8825, (*Node number cannot be read *)
		ERR_CAN_NILPTR := 8826, (*An invalid parameter has been transferred (NULL)*)
		ERR_CAN_IRQHANDLE := 8827, (*Internal management is destroyed in the interrupt handler*) (* this is not the complete list yet *)
		ERR_NO_DATA := 8877 (*No current data available for the specified CAN identifier.*)
		);
	eManagementByte : 
		( (* the or with 16#30 is the datalength OF 4 bytes for the managementbyte see SEW documentation  *)
		NO_SERVICE := 48, (* 0 OR 16#30 ==> 48 *)
		READ_PARAMETER := 49, (* 1 OR 16#30 ==> 49 *)
		WRITE_PARAMETER_NON_VOLATILE := 50, (* 2 OR 16#30 ==> 50 *)
		WRITE_PARAMETER_VOLATILE := 51, (* 3 OR 16#30 ==> 51 *)
		READ_MINIMUM := 52, (* 4 OR 16#30 ==> 52 *)
		READ_MAXIMUM := 53, (* 5 OR 16#30 ==> 53 *)
		READ_DEFAULT := 54, (* 6 OR 16#30 ==> 54 *)
		READ_SCALE := 55, (* 7 OR 16#30 ==> 55 *)
		READ_ATTRIBUTE := 56 (* 8 OR 16#30 ==> 56 *)
		);
	sbus_controller_internal_typ : 	STRUCT 
		state : UINT;
		CANwrite_0 : CANwrite;
		CANread_0 : CANread;
		byLowPo : ARRAY[0..3]OF USINT;
		byHighPo : ARRAY[0..3]OF USINT;
		byLowPi : ARRAY[0..2]OF USINT;
		byHighPi : ARRAY[0..3]OF USINT;
		abDataPo : ARRAY[0..7]OF USINT;
	END_STRUCT;
END_TYPE
