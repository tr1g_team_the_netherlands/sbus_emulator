PROGRAM _CYCLIC
(* GRAPHIC_INFORMATION_27DEE8E8_593B_45A1_8A15_7A695968B886_BEGIN *)
1CFCFBD-EditorCfcChart5.004.031
CCFCDoc1

CBaseDoc0

CPageFormat100NO286
CProject6

CCfgEditObject1-1
CTRrefdatatypes10
CDataType0BOOL15
CDataType0STRING[80]40
CDataType0USINT20
CDataType0UINT21
CDataType0UDINT22
CDataType0DINT18
CDataType0INFO_typ40
CDataType0ARRAY [ 0..2] OF INT40
CDataType0STRING[80]40
CDataType0ARRAY [ 0..7] OF BYTE40
CTRrefblocktypes3
CFirmwareBlockType0

CBlockType4NONONO10YES

CCfgEditObject1sBus_MasterDriver-1
CTRinputtype4
CClass2NOYES0NOBOOLNO

CCfgEditObject1xEnable1
CClass2NOYES1NOSTRING[80]NO

CCfgEditObject1strNetId2
CClass2NOYES2NOUSINTNO

CCfgEditObject1usiBaudRate3
CClass2NOYES3NOUINTNO

CCfgEditObject1uiMessages4
CTRoutputtype2
CClass2NONO0NOUINTNO

CCfgEditObject1uiStatus5
CClass2NONO1NOUDINTNO

CCfgEditObject1udiHandle6
CTRparamtype0
CFirmwareBlockType0

CBlockType4NONONO10YES

CCfgEditObject1sBus_info-1
CTRinputtype2
CClass2NOYES0NOBOOLNO

CCfgEditObject1xEnable1
CClass2NOYES1NODINTNO

CCfgEditObject1udiHandle2
CTRoutputtype2
CClass2NONO0NOUINTNO

CCfgEditObject1uiStatus3
CClass2NONO1NOINFO_typNO

CCfgEditObject1structInfo4
CTRparamtype0
CFirmwareBlockType0

CBlockType4NONONO10YES

CCfgEditObject1sbus_controller-1
CTRinputtype4
CClass2NOYES0NOARRAY [ 0..2] OF INTNO

CCfgEditObject1aiPo1
CClass2NOYES1NOUDINTNO

CCfgEditObject1udiHandle2
CClass2NOYES2NOUINTNO

CCfgEditObject1uiMotorNumber3
CClass2NOYES3NOUINTNO

CCfgEditObject1uiMasterStatus4
CTRoutputtype4
CClass2NONO0NOARRAY [ 0..2] OF INTNO

CCfgEditObject1aiPi5
CClass2NONO1NOSTRING[80]NO

CCfgEditObject1RandDmessage6
CClass2NONO2NOUINTNO

CCfgEditObject1uiStatus7
CClass2NONO3NOARRAY [ 0..7] OF BYTENO

CCfgEditObject1abDataPi8
CTRparamtype0
CPageFormat100NO286
CExtend_IEC_Info00
CFunctionChart40NO

CCfgEditObject1-1
CPageFormat100NO286
CTRblocks1
6Top Level Chart
CFreeCompoundBlockInterfaceType0

CBlockType4NONONO30YES

CCfgEditObject1-1
CTRinputtype0
CTRoutputtype0
CTRparamtype0
CFreeCompoundBlock1NO

CUserBlock0

COrganisationBlock0

CBlock7000111NO

CCfgEditObject1Top Level ChartTop Level ChartTop Level Chart10
CTRoutputs0
CTRinputs0
CTRparams0
CFunctionChart41NO

CCfgEditObject1-1
CPageFormat100NO286
CTRblocks4
0sBus_MasterDriversBus_MasterDriver
CFirmwareBlock0

CBlock7111431NO

CCfgEditObject1sBus_MasterDriver1sBus_MasterDriver10
CTRoutputs2
5
CConnector4YESNO-1NO

CCfgEditObject1uiStatusuiStatus5
CValue1
CValueItem121NO
CTRconnections2
CTempSimpleAddress042510
CTempSimpleAddress042010
6
CConnector4YESNO-1NO

CCfgEditObject1udiHandleudiHandle6
CValue1
CValueItem122NO
CTRconnections3
CTempSimpleAddress021510
CTempSimpleAddress022510
CTempSimpleAddress022010
CTRinputs4
1
CConnector4YESNO-1NO

CCfgEditObject1xEnablexEnable1
CValue1
CValueItem1xEnable15NO
CTRconnections0
2
CConnector4YESNO-1NO

CCfgEditObject1strNetIdstrNetId2
CValue1
CValueItem1'SS1.IF1'40NO
CTRconnections0
3
CConnector4YESNO-1NO

CCfgEditObject1usiBaudRateusiBaudRate3
CValue1
CValueItem15020NO
CTRconnections0
4
CConnector4YESNO-1NO

CCfgEditObject1uiMessagesuiMessages4
CValue1
CValueItem1521NO
CTRconnections0
CTRparams0
0sBus_infosBus_info
CFirmwareBlock0

CBlock71111522NO

CCfgEditObject1sBus_info1sBus_info15
CTRoutputs2
3
CConnector4YESNO-1NO

CCfgEditObject1uiStatusuiStatus3
CValue1
CValueItem1uiStatusSbusInfo21NO
CTRconnections0
4
CConnector4YESNO-1NO

CCfgEditObject1structInfostructInfo4
CValue1
CValueItem1structBusInfo40NO
CTRconnections0
CTRinputs2
1
CConnector4YESNO-1NO

CCfgEditObject1xEnablexEnable1
CValue1
CValueItem1xEnable15NO
CTRconnections0
2
CConnector4YESNO-1NO

CCfgEditObject1udiHandleudiHandle2
CValue1
CValueItem118NO
CTRconnections1
CTempSimpleAddress061010
CTRparams0
0sbus_controllersbus_controller
CFirmwareBlock0

CBlock72114633NO

CCfgEditObject1sbus_controller2sbus_controller20
CTRoutputs4
5
CConnector4YESNO-1NO

CCfgEditObject1aiPiaiPi5
CValue1
CValueItem1aiPiUser140NO
CTRconnections0
6
CConnector4YESNO-1NO

CCfgEditObject1RandDmessageRandDmessage6
CValue1
CValueItem140NO
CTRconnections0
7
CConnector4YESNO-1NO

CCfgEditObject1uiStatusuiStatus7
CValue1
CValueItem1uiStatusSbuscontrollerMotor121NO
CTRconnections0
8
CConnector4YESNO-1NO

CCfgEditObject1abDataPiabDataPi8
CValue1
CValueItem140NO
CTRconnections0
CTRinputs4
1
CConnector4YESNO-1NO

CCfgEditObject1aiPoaiPo1
CValue1
CValueItem1aiPoUser140NO
CTRconnections0
2
CConnector4YESNO-1NO

CCfgEditObject1udiHandleudiHandle2
CValue1
CValueItem122NO
CTRconnections1
CTempSimpleAddress061010
3
CConnector4YESNO-1NO

CCfgEditObject1uiMotorNumberuiMotorNumber3
CValue1
CValueItem1121NO
CTRconnections0
4
CConnector4YESNO-1NO

CCfgEditObject1uiMasterStatusuiMasterStatus4
CValue1
CValueItem121NO
CTRconnections1
CTempSimpleAddress051010
CTRparams0
0sbus_controllersbus_controller
CFirmwareBlock0

CBlock71118634NO

CCfgEditObject1sbus_controller1sbus_controller25
CTRoutputs4
5
CConnector4YESNO-1NO

CCfgEditObject1aiPiaiPi5
CValue1
CValueItem1aiPiUser240NO
CTRconnections0
6
CConnector4YESNO-1NO

CCfgEditObject1RandDmessageRandDmessage6
CValue1
CValueItem140NO
CTRconnections0
7
CConnector4YESNO-1NO

CCfgEditObject1uiStatusuiStatus7
CValue1
CValueItem1uiStatusSbuscontrollerMotor221NO
CTRconnections0
8
CConnector4YESNO-1NO

CCfgEditObject1abDataPiabDataPi8
CValue1
CValueItem1testReceive40NO
CTRconnections0
CTRinputs4
1
CConnector4YESNO-1NO

CCfgEditObject1aiPoaiPo1
CValue1
CValueItem1aiPoUser240NO
CTRconnections0
2
CConnector4YESNO-1NO

CCfgEditObject1udiHandleudiHandle2
CValue1
CValueItem122NO
CTRconnections1
CTempSimpleAddress061010
3
CConnector4YESNO-1NO

CCfgEditObject1uiMotorNumberuiMotorNumber3
CValue1
CValueItem1221NO
CTRconnections0
4
CConnector4YESNO-1NO

CCfgEditObject1uiMasterStatusuiMasterStatus4
CValue1
CValueItem121NO
CTRconnections1
CTempSimpleAddress051010
CTRparams0
CTRmargblocks3
501
CMarginBlock0

CBlock700011NO

CCfgEditObject11
CTRoutputs0
CTRinputs0
CTRparams0
502
CMarginBlock0

CBlock700011NO

CCfgEditObject12
CTRoutputs0
CTRinputs0
CTRparams0
503
CMarginBlock0

CBlock700011NO

CCfgEditObject13
CTRoutputs0
CTRinputs0
CTRparams0
CTRmargblocks2
501
CMarginBlock0

CBlock700011NO

CCfgEditObject11
CTRoutputs0
CTRinputs0
CTRparams0
502
CMarginBlock0

CBlock700011NO

CCfgEditObject12
CTRoutputs0
CTRinputs0
CTRparams0NO
(* GRAPHIC_INFORMATION_27DEE8E8_593B_45A1_8A15_7A695968B886_END *)
VAR
	zz0000FB_10_1_sBus_MasterDriver : sBus_MasterDriver;
	zz0000FB_10_1_sBus_info : sBus_info;
	zz0000FB_10_2_sbus_controller : sbus_controller;
	zz0000FB_10_1_sbus_controller : sbus_controller;
END_VAR

LD xEnable
ST zz0000FB_10_1_sBus_MasterDriver.xEnable
LD 'SS1.IF1'
ST zz0000FB_10_1_sBus_MasterDriver.strNetId
LD 50
ST zz0000FB_10_1_sBus_MasterDriver.usiBaudRate
LD 5
ST zz0000FB_10_1_sBus_MasterDriver.uiMessages
CAL zz0000FB_10_1_sBus_MasterDriver
LD xEnable
ST zz0000FB_10_1_sBus_info.xEnable
LD zz0000FB_10_1_sBus_MasterDriver.udiHandle
ST zz0000FB_10_1_sBus_info.udiHandle
CAL zz0000FB_10_1_sBus_info
LD zz0000FB_10_1_sBus_info.uiStatus
ST uiStatusSbusInfo
LD zz0000FB_10_1_sBus_info.structInfo
ST structBusInfo
LD aiPoUser1
ST zz0000FB_10_2_sbus_controller.aiPo
LD zz0000FB_10_1_sBus_MasterDriver.udiHandle
ST zz0000FB_10_2_sbus_controller.udiHandle
LD 1
ST zz0000FB_10_2_sbus_controller.uiMotorNumber
LD zz0000FB_10_1_sBus_MasterDriver.uiStatus
ST zz0000FB_10_2_sbus_controller.uiMasterStatus
CAL zz0000FB_10_2_sbus_controller
LD zz0000FB_10_2_sbus_controller.aiPi
ST aiPiUser1
LD zz0000FB_10_2_sbus_controller.uiStatus
ST uiStatusSbuscontrollerMotor1
LD aiPoUser2
ST zz0000FB_10_1_sbus_controller.aiPo
LD zz0000FB_10_1_sBus_MasterDriver.udiHandle
ST zz0000FB_10_1_sbus_controller.udiHandle
LD 2
ST zz0000FB_10_1_sbus_controller.uiMotorNumber
LD zz0000FB_10_1_sBus_MasterDriver.uiStatus
ST zz0000FB_10_1_sbus_controller.uiMasterStatus
CAL zz0000FB_10_1_sbus_controller
LD zz0000FB_10_1_sbus_controller.aiPi
ST aiPiUser2
LD zz0000FB_10_1_sbus_controller.uiStatus
ST uiStatusSbuscontrollerMotor2
LD zz0000FB_10_1_sbus_controller.abDataPi
ST testReceive


END_PROGRAM
