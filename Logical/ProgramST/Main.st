
PROGRAM _INIT
	
	sBus_MasterDriver_0(xEnable := TRUE, strNetId := 'SS1.IF1', usiBaudRate := 50, uiMessages := 5);
	 
END_PROGRAM

PROGRAM _CYCLIC
	
	sBus_info_0(xEnable := TRUE, udiHandle := sBus_MasterDriver_0.udiHandle);
	sbus_controller_0(aiPo := aiPiUser1, udiHandle := sBus_MasterDriver_0.udiHandle, uiMotorNumber := 1, uiMasterStatus := sBus_info_0.uiStatus);
	 
END_PROGRAM

PROGRAM _EXIT
	
	
	 
END_PROGRAM

