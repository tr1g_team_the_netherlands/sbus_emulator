/* Automation Studio generated header file */
/* Do not edit ! */
/* Sbus_lib  */

#ifndef _SBUS_LIB_
#define _SBUS_LIB_
#ifdef __cplusplus
extern "C" 
{
#endif

#include <bur/plctypes.h>

#ifndef _BUR_PUBLIC
#define _BUR_PUBLIC
#endif
/* Datatypes and datatypes of function blocks */
typedef enum CAN_lib_ErrorNumbers
{	NO_ERROR = 0,
	FBK_STILL_ACTIVE = 1,
	NAME_NOT_AVAILIBLE = 3019,
	DEVICE_NOT_ALLOWED = 3090,
	ERR_DEVICEDESCRIPTION = 8252,
	ERR_CAN_NOENTRY = 8800,
	ERR_CAN_NOMEM = 8801,
	ERR_CAN_WRIDENT = 8802,
	ERR_CAN_COBUSED = 8803,
	ERR_CAN_WRCANID = 8804,
	ERR_CAN_WRUSERID = 8805,
	ERR_CAN_COBDEF = 8806,
	ERR_CAN_WRDEF = 8807,
	ERR_CAN_WRHDLE = 8808,
	ERR_CAN_BUSY = 8809,
	ERR_CAN_QFULL = 8810,
	ERR_CAN_BUSOFF = 8811,
	ERR_CAN_NOHIGH = 8812,
	ERR_CAN_NOTINST = 8813,
	ERR_CAN_WRLNG = 8814,
	ERR_CAN_BDRATE = 8815,
	ERR_CAN_COBANZ = 8816,
	ERR_CAN_NOCONTR = 8817,
	ERR_CAN_HTLXINF = 8818,
	ERR_CAN_HTLQUIT = 8819,
	ERR_CAN_NOFUNC = 8820,
	ERR_CAN_WRUSRTYP = 8821,
	ERR_WRCMSOBJ = 8822,
	ERR_CAN_WRSTRUCT = 8823,
	ERR_CAN_WRMODE = 8824,
	ERR_CAN_NO_NODE = 8825,
	ERR_CAN_NILPTR = 8826,
	ERR_CAN_IRQHANDLE = 8827,
	ERR_NO_DATA = 8877
} CAN_lib_ErrorNumbers;

typedef enum eManagementByte
{	NO_SERVICE = 48,
	READ_PARAMETER = 49,
	WRITE_PARAMETER_NON_VOLATILE = 50,
	WRITE_PARAMETER_VOLATILE = 51,
	READ_MINIMUM = 52,
	READ_MAXIMUM = 53,
	READ_DEFAULT = 54,
	READ_SCALE = 55,
	READ_ATTRIBUTE = 56
} eManagementByte;

typedef struct sBus_MasterDriver
{
	/* VAR_INPUT (analog) */
	plcstring strNetId[81];
	unsigned char usiBaudRate;
	unsigned short uiMessages;
	/* VAR_OUTPUT (analog) */
	unsigned short uiStatus;
	unsigned long udiHandle;
	/* VAR (analog) */
	unsigned short uiErrorAddress;
	struct CANopen sBusOpen;
	/* VAR_INPUT (digital) */
	plcbit xEnable;
} sBus_MasterDriver_typ;

typedef struct sBus_info
{
	/* VAR_INPUT (analog) */
	signed long udiHandle;
	/* VAR_OUTPUT (analog) */
	unsigned short uiStatus;
	struct INFO_typ structInfo;
	/* VAR (analog) */
	struct CANinfo CANinfo_0;
	struct INFO_typ infoStruct;
	/* VAR_INPUT (digital) */
	plcbit xEnable;
} sBus_info_typ;

typedef struct sbus_controller
{
	/* VAR_INPUT (analog) */
	signed short aiPo[3];
	unsigned long udiHandle;
	unsigned short uiMotorNumber;
	unsigned short uiMasterStatus;
	/* VAR_OUTPUT (analog) */
	signed short aiPi[3];
	plcstring RandDmessage[81];
	unsigned short uiStatus;
	plcbyte abDataPi[8];
	/* VAR (analog) */
	plcbyte byLowPo[4];
	plcbyte byHighPo[4];
	plcbyte byLowPi[4];
	plcbyte byHighPi[4];
	plcbyte abDataPo[8];
	struct CANwrite CANwrite_01;
	struct CANread CANread_0;
} sbus_controller_typ;



/* Prototyping of functions and function blocks */
_BUR_PUBLIC void sBus_MasterDriver(struct sBus_MasterDriver* inst);
_BUR_PUBLIC void sBus_info(struct sBus_info* inst);
_BUR_PUBLIC void sbus_controller(struct sbus_controller* inst);
_BUR_PUBLIC signed short jointTheBytes(plcbyte byHigh, plcbyte byLow);
_BUR_PUBLIC plcbit splitTheInt(signed short iSource, plcbyte* byLow, plcbyte* byHigh);
_BUR_PUBLIC unsigned short Sbus_IdPi(unsigned short uiMotorNumber);
_BUR_PUBLIC unsigned short Sbus_IdPo(unsigned short uiMotorNumber);


#ifdef __cplusplus
};
#endif
#endif /* _SBUS_LIB_ */

