UnmarkedObjectFolder := C:/Users/patrick.vanieperen/Documents/Diverse Ontwikkelingen/Marel Stork/TR1G/Software/Sbus_emulator/Logical/Program
MarkedObjectFolder := C:/Users/patrick.vanieperen/Documents/Diverse\ Ontwikkelingen/Marel\ Stork/TR1G/Software/Sbus_emulator/Logical/Program

$(AS_CPU_PATH)/Program.br: \
	$(AS_PROJECT_CPU_PATH)/Cpu.per \
	$(AS_CPU_PATH)/Program/Program.ox
	@"$(AS_BIN_PATH)/BR.AS.TaskBuilder.exe" "$(AS_CPU_PATH)/Program/Program.ox" -o "$(AS_CPU_PATH)/Program.br" -v V1.00.0 -f "$(AS_CPU_PATH)/NT.ofs" -offsetLT "$(AS_BINARIES_PATH)/$(AS_CONFIGURATION)/$(AS_PLC)/LT.ofs" -T SG4  -M IA32  -B I4.25 -extConstants -d "runtime: V* - V*,asieccon: V* - V*" -r Cyclic4 -p 2 -s "Program" -L "AsIecCon: V*, astime: V*, CAN_Lib: V*, Operator: V*, Runtime: V*, Sbus_lib: V*" -P "$(AS_PROJECT_PATH)" -secret "$(AS_PROJECT_PATH)_br.as.taskbuilder.exe"

$(AS_CPU_PATH)/Program/Program.ox: \
	$(AS_CPU_PATH)/Program/a.out
	@"$(AS_BIN_PATH)/BR.AS.Backend.exe" "$(AS_CPU_PATH)/Program/a.out" -o "$(AS_CPU_PATH)/Program/Program.ox" -T SG4 -r Cyclic4   -G V4.1.2  -secret "$(AS_PROJECT_PATH)_br.as.backend.exe"

$(AS_CPU_PATH)/Program/a.out: \
	$(AS_CPU_PATH)/Program/Cyclic.cfc.o \
	$(AS_CPU_PATH)/Program/Init.cfc.o \
	$(AS_CPU_PATH)/Program/Exit.cfc.o
	@"$(AS_BIN_PATH)/BR.AS.CCompiler.exe" -link "$(AS_CPU_PATH)/Program/Cyclic.cfc.o" "$(AS_CPU_PATH)/Program/Init.cfc.o" "$(AS_CPU_PATH)/Program/Exit.cfc.o"  -o "$(AS_CPU_PATH)/Program/a.out"  -G V4.1.2  -T SG4  -M IA32  "-Wl,$(AS_SYSTEM_PATH)/I0425/SG4/IA32/libAsIecCon.a" "-Wl,$(AS_TEMP_PATH)/Archives/$(AS_CONFIGURATION)/$(AS_PLC)/libSbus_lib.a" "-Wl,$(AS_SYSTEM_PATH)/I0425/SG4/IA32/libCAN_Lib.a" "-Wl,$(AS_SYSTEM_PATH)/I0425/SG4/IA32/libastime.a" "-Wl,$(AS_SYSTEM_PATH)/I0425/SG4/IA32/liboperator.a" "-Wl,$(AS_SYSTEM_PATH)/I0425/SG4/IA32/libruntime.a" -specs=I386specs -Wl,-q,-T,bur_elf_i386.x -nostdlib -secret "$(AS_PROJECT_PATH)_br.as.ccompiler.exe"

$(AS_CPU_PATH)/Program/Cyclic.cfc.o: \
	$(AS_PROJECT_PATH)/Logical/Program/Cyclic.cfc \
	$(AS_PROJECT_PATH)/Logical/Libraries/Sbus_lib/Sbus_lib.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/CAN_Lib/CAN_Lib.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/CAN_Lib/CAN_Lib.typ \
	$(AS_PROJECT_PATH)/Logical/Program/Variables.var \
	$(AS_PROJECT_PATH)/Logical/Libraries/Sbus_lib/Types.typ
	@"$(AS_BIN_PATH)/BR.AS.IecCompiler.exe" "$(AS_PROJECT_PATH)/Logical/Program/Cyclic.cfc" -o "$(AS_CPU_PATH)/Program/Cyclic.cfc.o"  -O "$(AS_CPU_PATH)//Program/Cyclic.cfc.o.opt" -secret "$(AS_PROJECT_PATH)_br.as.ieccompiler.exe"

$(AS_CPU_PATH)/Program/Init.cfc.o: \
	$(AS_PROJECT_PATH)/Logical/Program/Init.cfc \
	$(AS_PROJECT_PATH)/Logical/Libraries/Sbus_lib/Sbus_lib.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/CAN_Lib/CAN_Lib.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/CAN_Lib/CAN_Lib.typ \
	$(AS_PROJECT_PATH)/Logical/Program/Variables.var \
	$(AS_PROJECT_PATH)/Logical/Libraries/Sbus_lib/Types.typ
	@"$(AS_BIN_PATH)/BR.AS.IecCompiler.exe" "$(AS_PROJECT_PATH)/Logical/Program/Init.cfc" -o "$(AS_CPU_PATH)/Program/Init.cfc.o"  -O "$(AS_CPU_PATH)//Program/Init.cfc.o.opt" -secret "$(AS_PROJECT_PATH)_br.as.ieccompiler.exe"

$(AS_CPU_PATH)/Program/Exit.cfc.o: \
	$(AS_PROJECT_PATH)/Logical/Program/Exit.cfc \
	$(AS_PROJECT_PATH)/Logical/Libraries/Sbus_lib/Sbus_lib.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/CAN_Lib/CAN_Lib.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/CAN_Lib/CAN_Lib.typ \
	$(AS_PROJECT_PATH)/Logical/Program/Variables.var \
	$(AS_PROJECT_PATH)/Logical/Libraries/Sbus_lib/Types.typ
	@"$(AS_BIN_PATH)/BR.AS.IecCompiler.exe" "$(AS_PROJECT_PATH)/Logical/Program/Exit.cfc" -o "$(AS_CPU_PATH)/Program/Exit.cfc.o"  -O "$(AS_CPU_PATH)//Program/Exit.cfc.o.opt" -secret "$(AS_PROJECT_PATH)_br.as.ieccompiler.exe"

-include $(AS_CPU_PATH)/Force.mak 

