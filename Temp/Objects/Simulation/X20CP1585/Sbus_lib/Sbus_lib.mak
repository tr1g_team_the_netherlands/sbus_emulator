UnmarkedObjectFolder := C:/Users/patrick.vanieperen/Documents/Diverse Ontwikkelingen/Marel Stork/TR1G/Software/Sbus_emulator/Logical/Libraries/Sbus_lib
MarkedObjectFolder := C:/Users/patrick.vanieperen/Documents/Diverse\ Ontwikkelingen/Marel\ Stork/TR1G/Software/Sbus_emulator/Logical/Libraries/Sbus_lib

$(AS_CPU_PATH)/Sbus_lib.br: \
	$(AS_PROJECT_PATH)/Logical/Libraries/Sbus_lib/IEC.lby \
	$(AS_CPU_PATH)/Sbus_lib/Sbus_lib.ox
	@"$(AS_BIN_PATH)/BR.AS.TaskBuilder.exe" "$(AS_CPU_PATH)/Sbus_lib/Sbus_lib.ox" -o "$(AS_CPU_PATH)/Sbus_lib.br" -v V1.00.0 -f "$(AS_CPU_PATH)/NT.ofs" -offsetLT "$(AS_BINARIES_PATH)/$(AS_CONFIGURATION)/$(AS_PLC)/LT.ofs" -T SG4  -M IA32  -B I4.25 -extConstants -d "runtime: V* - V*,asieccon: V* - V*" -r Library -s "Libraries.Sbus_lib" -L "AsIecCon: V*, astime: V*, CAN_Lib: V*, Operator: V*, Runtime: V*, Sbus_lib: V*" -P "$(AS_PROJECT_PATH)" -secret "$(AS_PROJECT_PATH)_br.as.taskbuilder.exe"

$(AS_CPU_PATH)/Sbus_lib/Sbus_lib.ox: \
	$(AS_CPU_PATH)/Sbus_lib/a.out
	@"$(AS_BIN_PATH)/BR.AS.Backend.exe" "$(AS_CPU_PATH)/Sbus_lib/a.out" -o "$(AS_CPU_PATH)/Sbus_lib/Sbus_lib.ox" -T SG4 -r Library   -G V4.1.2  -secret "$(AS_PROJECT_PATH)_br.as.backend.exe"

$(AS_CPU_PATH)/Sbus_lib/a.out: \
	$(AS_CPU_PATH)/Sbus_lib/jointTheBytes.st.o \
	$(AS_CPU_PATH)/Sbus_lib/splitTheInt.st.o \
	$(AS_CPU_PATH)/Sbus_lib/Sbus_IdPo.st.o \
	$(AS_CPU_PATH)/Sbus_lib/Sbus_IdPi.st.o \
	$(AS_CPU_PATH)/Sbus_lib/sBus_info.st.o \
	$(AS_CPU_PATH)/Sbus_lib/sbus_controller.st.o \
	$(AS_CPU_PATH)/Sbus_lib/sBus_MasterDriver.st.o
	@"$(AS_BIN_PATH)/BR.AS.CCompiler.exe" -link "$(AS_CPU_PATH)/Sbus_lib/jointTheBytes.st.o" "$(AS_CPU_PATH)/Sbus_lib/splitTheInt.st.o" "$(AS_CPU_PATH)/Sbus_lib/Sbus_IdPo.st.o" "$(AS_CPU_PATH)/Sbus_lib/Sbus_IdPi.st.o" "$(AS_CPU_PATH)/Sbus_lib/sBus_info.st.o" "$(AS_CPU_PATH)/Sbus_lib/sbus_controller.st.o" "$(AS_CPU_PATH)/Sbus_lib/sBus_MasterDriver.st.o"  -o "$(AS_CPU_PATH)/Sbus_lib/a.out"  -G V4.1.2  -T SG4  -M IA32  "-Wl,$(AS_SYSTEM_PATH)/I0425/SG4/IA32/libAsIecCon.a" "-Wl,$(AS_SYSTEM_PATH)/I0425/SG4/IA32/libCAN_Lib.a" "-Wl,$(AS_SYSTEM_PATH)/I0425/SG4/IA32/libastime.a" "-Wl,$(AS_SYSTEM_PATH)/I0425/SG4/IA32/liboperator.a" "-Wl,$(AS_SYSTEM_PATH)/I0425/SG4/IA32/libruntime.a" -specs=I386specs -Wl,-q,-T,bur_elf_i386.x -nostdlib -secret "$(AS_PROJECT_PATH)_br.as.ccompiler.exe"

$(AS_CPU_PATH)/Sbus_lib/jointTheBytes.st.o: \
	$(AS_PROJECT_PATH)/Logical/Libraries/Sbus_lib/Functions/jointTheBytes.st \
	$(AS_PROJECT_PATH)/Logical/Libraries/Sbus_lib/Sbus_lib.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/CAN_Lib/CAN_Lib.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/CAN_Lib/CAN_Lib.typ
	@"$(AS_BIN_PATH)/BR.AS.IecCompiler.exe" "$(AS_PROJECT_PATH)/Logical/Libraries/Sbus_lib/Functions/jointTheBytes.st" -o "$(AS_CPU_PATH)/Sbus_lib/jointTheBytes.st.o"  -O "$(AS_CPU_PATH)//Sbus_lib/jointTheBytes.st.o.opt" -secret "$(AS_PROJECT_PATH)_br.as.ieccompiler.exe"

$(AS_CPU_PATH)/Sbus_lib/splitTheInt.st.o: \
	$(AS_PROJECT_PATH)/Logical/Libraries/Sbus_lib/Functions/splitTheInt.st \
	$(AS_PROJECT_PATH)/Logical/Libraries/Sbus_lib/Sbus_lib.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/CAN_Lib/CAN_Lib.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/CAN_Lib/CAN_Lib.typ
	@"$(AS_BIN_PATH)/BR.AS.IecCompiler.exe" "$(AS_PROJECT_PATH)/Logical/Libraries/Sbus_lib/Functions/splitTheInt.st" -o "$(AS_CPU_PATH)/Sbus_lib/splitTheInt.st.o"  -O "$(AS_CPU_PATH)//Sbus_lib/splitTheInt.st.o.opt" -secret "$(AS_PROJECT_PATH)_br.as.ieccompiler.exe"

$(AS_CPU_PATH)/Sbus_lib/Sbus_IdPo.st.o: \
	$(AS_PROJECT_PATH)/Logical/Libraries/Sbus_lib/Functions/Sbus_IdPo.st \
	$(AS_PROJECT_PATH)/Logical/Libraries/Sbus_lib/Sbus_lib.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/CAN_Lib/CAN_Lib.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/CAN_Lib/CAN_Lib.typ
	@"$(AS_BIN_PATH)/BR.AS.IecCompiler.exe" "$(AS_PROJECT_PATH)/Logical/Libraries/Sbus_lib/Functions/Sbus_IdPo.st" -o "$(AS_CPU_PATH)/Sbus_lib/Sbus_IdPo.st.o"  -O "$(AS_CPU_PATH)//Sbus_lib/Sbus_IdPo.st.o.opt" -secret "$(AS_PROJECT_PATH)_br.as.ieccompiler.exe"

$(AS_CPU_PATH)/Sbus_lib/Sbus_IdPi.st.o: \
	$(AS_PROJECT_PATH)/Logical/Libraries/Sbus_lib/Functions/Sbus_IdPi.st \
	$(AS_PROJECT_PATH)/Logical/Libraries/Sbus_lib/Sbus_lib.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/CAN_Lib/CAN_Lib.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/CAN_Lib/CAN_Lib.typ
	@"$(AS_BIN_PATH)/BR.AS.IecCompiler.exe" "$(AS_PROJECT_PATH)/Logical/Libraries/Sbus_lib/Functions/Sbus_IdPi.st" -o "$(AS_CPU_PATH)/Sbus_lib/Sbus_IdPi.st.o"  -O "$(AS_CPU_PATH)//Sbus_lib/Sbus_IdPi.st.o.opt" -secret "$(AS_PROJECT_PATH)_br.as.ieccompiler.exe"

$(AS_CPU_PATH)/Sbus_lib/sBus_info.st.o: \
	$(AS_PROJECT_PATH)/Logical/Libraries/Sbus_lib/FunctionBlocks/sBus_info.st \
	$(AS_PROJECT_PATH)/Logical/Libraries/Sbus_lib/Sbus_lib.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/CAN_Lib/CAN_Lib.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/CAN_Lib/CAN_Lib.typ
	@"$(AS_BIN_PATH)/BR.AS.IecCompiler.exe" "$(AS_PROJECT_PATH)/Logical/Libraries/Sbus_lib/FunctionBlocks/sBus_info.st" -o "$(AS_CPU_PATH)/Sbus_lib/sBus_info.st.o"  -O "$(AS_CPU_PATH)//Sbus_lib/sBus_info.st.o.opt" -secret "$(AS_PROJECT_PATH)_br.as.ieccompiler.exe"

$(AS_CPU_PATH)/Sbus_lib/sbus_controller.st.o: \
	$(AS_PROJECT_PATH)/Logical/Libraries/Sbus_lib/FunctionBlocks/sbus_controller.st \
	FORCE 
	@"$(AS_BIN_PATH)/BR.AS.IecCompiler.exe" "$(AS_PROJECT_PATH)/Logical/Libraries/Sbus_lib/FunctionBlocks/sbus_controller.st" -o "$(AS_CPU_PATH)/Sbus_lib/sbus_controller.st.o"  -O "$(AS_CPU_PATH)//Sbus_lib/sbus_controller.st.o.opt" -secret "$(AS_PROJECT_PATH)_br.as.ieccompiler.exe"

$(AS_CPU_PATH)/Sbus_lib/sBus_MasterDriver.st.o: \
	$(AS_PROJECT_PATH)/Logical/Libraries/Sbus_lib/FunctionBlocks/sBus_MasterDriver.st \
	FORCE 
	@"$(AS_BIN_PATH)/BR.AS.IecCompiler.exe" "$(AS_PROJECT_PATH)/Logical/Libraries/Sbus_lib/FunctionBlocks/sBus_MasterDriver.st" -o "$(AS_CPU_PATH)/Sbus_lib/sBus_MasterDriver.st.o"  -O "$(AS_CPU_PATH)//Sbus_lib/sBus_MasterDriver.st.o.opt" -secret "$(AS_PROJECT_PATH)_br.as.ieccompiler.exe"

-include $(AS_CPU_PATH)/Force.mak 



FORCE: