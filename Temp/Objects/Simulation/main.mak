SHELL = cmd.exe
CYGWIN=nontsec
export PATH := C:\ProgramData\Oracle\Java\javapath;C:\Program Files\SynView\bin;C:\Program Files (x86)\KeyTalk\;C:\Program Files\Common Files\Microsoft Shared\Microsoft Online Services;C:\Program Files (x86)\Common Files\Microsoft Shared\Microsoft Online Services;C:\Program Files\Common Files\Microsoft Shared\Windows Live;C:\Program Files (x86)\Common Files\Microsoft Shared\Windows Live;C:\Windows\system32;C:\Windows;C:\Windows\System32\Wbem;C:\Program Files (x86)\Microsoft Application Virtualization Client;C:\Program Files (x86)\Windows Live\Shared;C:\Program Files\Adobe\Adobe PDF iFilter 9 for 64-bit platforms\bin\;C:\Program Files\TortoiseSVN\bin;C:\Program Files\Mercurial\;C:\MinGW\bin;C:\Program Files\doxygen\bin;C:\MinGW\include;C:\Program Files\MATLAB\R2015b\runtime\win64;C:\Program Files\MATLAB\R2015b\bin;C:\Windows\System32\WindowsPowerShell\v1.0\;C:\Users\patrick.vanieperen\Documents\Diverse Ontwikkelingen\Matlab packages\TDM-GCC-64\bin;C:\TDM-GCC-64\bin;C:\Program Files\TortoiseHg\;C:\Program Files (x86)\Microsoft SQL Server\Client SDK\ODBC\130\Tools\Binn\;C:\Program Files (x86)\Microsoft SQL Server\130\Tools\Binn\;C:\Program Files (x86)\Microsoft SQL Server\130\DTS\Binn\;C:\Program Files (x86)\Microsoft SQL Server\130\Tools\Binn\ManagementStudio\;C:\Program Files (x86)\Microsoft SQL Server\Client SDK\ODBC\110\Tools\Binn\;C:\Program Files (x86)\Microsoft SQL Server\120\Tools\Binn\;C:\Program Files (x86)\Microsoft SQL Server\120\DTS\Binn\;C:\Program Files (x86)\Microsoft SQL Server\120\Tools\Binn\ManagementStudio\;C:\Program Files (x86)\Skype\Phone\;C:\Program Files (x86)\CodeSmith\v7.1\;C:\Program Files\TortoiseGit\bin;C:\Program Files\CMake\bin;C:\opencv310\release\bin;C:\Program Files\SynView\bin;C:\Users\patrick.vanieperen\AppData\Local\Programs\Python\Python35\Scripts\;C:\Users\patrick.vanieperen\AppData\Local\Programs\Python\Python35\;C:\Users\patrick.vanieperen\AppData\Local\Programs\Python\Python35-32\Scripts\;C:\Users\patrick.vanieperen\AppData\Local\Programs\Python\Python35-32\;C:\Program Files (x86)\Internet Explorer;C:\Program Files\Common Files\Microsoft Shared\Microsoft Online Services;C:\Program Files (x86)\Common Files\Microsoft Shared\Microsoft Online Services;c:\Program Files (x86)\KeyTalk\;C:\Program Files\Common Files\Microsoft Shared\Windows Live;C:\Program Files (x86)\Common Files\Microsoft Shared\Windows Live;C:\Windows\system32;C:\Windows;C:\Windows\System32\Wbem;C:\Windows\System32\WindowsPowerShell\v1.0\;C:\Program Files (x86)\Microsoft Application Virtualization Client;C:\Program Files (x86)\Windows Live\Shared;C:\Program Files\Adobe\Adobe PDF iFilter 9 for 64-bit platforms\bin\;C:\Program Files\TortoiseSVN\bin;C:\Program Files\Mercurial\;C:\MinGW\bin;C:\Program Files\TortoiseGit\bin;C:\TDM-GCC-64\bin;C:\Users\patrick.vanieperen\AppData\Local\Programs\Python\Python35\Scripts\;C:\Users\patrick.vanieperen\AppData\Local\Programs\Python\Python35\;C:\Users\patrick.vanieperen\AppData\Local\Programs\Python\Python35-32\Scripts\;C:\Users\patrick.vanieperen\AppData\Local\Programs\Python\Python35-32\;C:\Program Files (x86)\Internet Explorer;C:\Program Files\Common Files\Microsoft Shared\Microsoft Online Services;C:\Program Files (x86)\Common Files\Microsoft Shared\Microsoft Online Services;c:\Program Files (x86)\KeyTalk\;C:\Program Files\Common Files\Microsoft Shared\Windows Live;C:\Program Files (x86)\Common Files\Microsoft Shared\Windows Live;C:\Windows\system32;C:\Windows;C:\Windows\System32\Wbem;C:\Windows\System32\WindowsPowerShell\v1.0\;C:\Program Files (x86)\Microsoft Application Virtualization Client;C:\Program Files (x86)\Windows Live\Shared;C:\Program Files\Adobe\Adobe PDF iFilter 9 for 64-bit platforms\bin\;C:\Program Files\TortoiseSVN\bin;C:\Program Files\Mercurial\;C:\MinGW\bin;C:\Program Files\TortoiseGit\bin;C:\TDM-GCC-64\bin
export AS_BUILD_MODE := BuildAndTransfer
export AS_VERSION := 4.2.5.383
export AS_COMPANY_NAME := Marel Food Systems
export AS_USER_NAME := patrick.vanieperen
export AS_PATH := C:/BR/BrAutomation_new/AS42
export AS_BIN_PATH := C:/BR/BrAutomation_new/AS42/Bin-en
export AS_PROJECT_PATH := C:/Users/patrick.vanieperen/Documents/Diverse\ Ontwikkelingen/Marel\ Stork/TR1G/Software/Sbus_emulator
export AS_PROJECT_NAME := Sbus_emulator
export AS_SYSTEM_PATH := C:/BR/BrAutomation_new/AS/System
export AS_VC_PATH := C:/BR/BrAutomation_new/AS42/AS/VC
export AS_TEMP_PATH := C:/Users/patrick.vanieperen/Documents/Diverse\ Ontwikkelingen/Marel\ Stork/TR1G/Software/Sbus_emulator/Temp
export AS_CONFIGURATION := Simulation
export AS_BINARIES_PATH := C:/Users/patrick.vanieperen/Documents/Diverse\ Ontwikkelingen/Marel\ Stork/TR1G/Software/Sbus_emulator/Binaries
export AS_GNU_INST_PATH := C:/BR/BrAutomation_new/AS42/AS/GnuInst/V4.1.2
export AS_GNU_BIN_PATH := $(AS_GNU_INST_PATH)/bin
export AS_GNU_INST_PATH_SUB_MAKE := C:/BR/BrAutomation_new/AS42/AS/GnuInst/V4.1.2
export AS_GNU_BIN_PATH_SUB_MAKE := $(AS_GNU_INST_PATH_SUB_MAKE)/bin
export AS_INSTALL_PATH := C:/BR/BrAutomation_new/AS42
export WIN32_AS_PATH := "C:\BR\BrAutomation_new\AS42"
export WIN32_AS_BIN_PATH := "C:\BR\BrAutomation_new\AS42\Bin-en"
export WIN32_AS_PROJECT_PATH := "C:\Users\patrick.vanieperen\Documents\Diverse Ontwikkelingen\Marel Stork\TR1G\Software\Sbus_emulator"
export WIN32_AS_SYSTEM_PATH := "C:\BR\BrAutomation_new\AS\System"
export WIN32_AS_VC_PATH := "C:\BR\BrAutomation_new\AS42\AS\VC"
export WIN32_AS_TEMP_PATH := "C:\Users\patrick.vanieperen\Documents\Diverse Ontwikkelingen\Marel Stork\TR1G\Software\Sbus_emulator\Temp"
export WIN32_AS_BINARIES_PATH := "C:\Users\patrick.vanieperen\Documents\Diverse Ontwikkelingen\Marel Stork\TR1G\Software\Sbus_emulator\Binaries"
export WIN32_AS_GNU_INST_PATH := "C:\BR\BrAutomation_new\AS42\AS\GnuInst\V4.1.2"
export WIN32_AS_GNU_BIN_PATH := "$(WIN32_AS_GNU_INST_PATH)\\bin" 
export WIN32_AS_INSTALL_PATH := "C:\BR\BrAutomation_new\AS42"

.suffixes:

ProjectMakeFile:

	@"$(AS_BIN_PATH)/BR.AS.AnalyseProject.exe" "$(AS_PROJECT_PATH)/Sbus_emulator.apj" -t "$(AS_TEMP_PATH)" -c "$(AS_CONFIGURATION)" -o "$(AS_BINARIES_PATH)"   -sfas -buildMode "BuildAndTransfer"  

	@$(AS_GNU_BIN_PATH)/mingw32-make.exe -r -f 'C:/Users/patrick.vanieperen/Documents/Diverse Ontwikkelingen/Marel Stork/TR1G/Software/Sbus_emulator/Temp/Objects/$(AS_CONFIGURATION)/X20CP1585/#cpu.mak' -k 

